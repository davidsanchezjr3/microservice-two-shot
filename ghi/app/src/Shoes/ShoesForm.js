import { useState, useEffect } from 'react'

export default function ShoesForm() {
    const[modelName, setModelName] = useState('');
    const[manufacturer, setManufacturer] = useState('');
    const[color, setColor] = useState('');
    const[pictureUrl, setPictureUrl] = useState('');
    const[bin, setBin] = useState('');
    const[bins, setBins] = useState([]);

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value)
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value)
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value)
    }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value)
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value)
    }

    useEffect(() => {
        async function getBins() {
            const url = 'http://localhost:8100/api/bins';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setBins(data.bins);
            }
        }
        getBins();
    }, []);

    // needs to be snake case
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.model_name = modelName;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;
        console.log(data);

        const shoeUrl = `http://localhost:8080/api/shoes/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json'
            }
        }
        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json();
            setModelName('')
            setManufacturer('')
            setColor('')
            setBin('')
            setPictureUrl('')
        }
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                    <input onChange={handleModelNameChange} value={modelName} placeholder="Name" required type="text" name="modelName" id="modelName" className="form-control"/>
                    <label htmlFor="modelName">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture" required type="text" name="pictureUrl" id="pictureUrl" className="form-control"/>
                    <label htmlFor="pictureUrl">Picture</label>
                </div>

                <div className="mb-3">
                    <select required onChange={handleBinChange} name="bin" id="bin" className="form-select" value={bin} >
                        <option value="">Choose a Bin</option>
                        {bins.map(bin => {
                            return (
                                <option key={bin.id} value={bin.href}>
                                    {bin.bin_number}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>

            </div>
        </div>
    </div>

    )
}
