import React, {useEffect, useState } from 'react';

export default function ShoeList () {
    const [shoes, setShoes] = useState([]);
    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes');

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        } else {
            console.error(response);
        }
    }

    const handleDelete = (id) => async (e) => {
        e.preventDefault();

        const deleteShoeURL = `http://localhost:8080/api/shoes/${id}/`;
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(deleteShoeURL, fetchConfig);
        if (response.ok) {
            console.log("You deleted a shoe.");
            fetchData();
        } else {
            console.error(response);
        }

    };

    useEffect(() => {
        fetchData();
    }, []);


    return(
    <table className = "table table-striped">
        <thead>
            <tr>
                <th>Manufacturer</th>
                <th>Name</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Bin</th>
                <th>Delete?</th>
            </tr>
        </thead>
        <tbody>
            {shoes?.map(shoe =>{
                return(
                    <tr key={ shoe.id }>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model_name }</td>
                        <td>{ shoe.color }</td>
                        <td><img src={ shoe.picture_url } width= '100' height= '100'></img></td>
                        <td>{ shoe.bin }</td>
                        <td><button onClick={handleDelete(shoe.id)} className="btn btn-sm btn-info">Delete</button></td>
                    </tr>
                )
            })}

        </tbody>
    </table>

    )
}
