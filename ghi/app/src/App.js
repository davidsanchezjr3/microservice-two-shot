import { BrowserRouter, Routes, Route } from 'react-router-dom';

import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './Shoes/ShoesList';
import ShoesForm from './Shoes/ShoesForm';
import HatsList from "./HatsList"
import CreateHat from './CreateHat';


export default function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList/>} />
            <Route path="new" element={<ShoesForm/>} />
          </Route>
          <Route path="hats">
            <Route path="" element={<HatsList />} />
            <Route path="new" element={<CreateHat />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
