import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

function HatsList() {
    const [hats, setHats] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats";

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


async function HatDelete(hat) {
    const hatDeleteUrl = `http://localhost:8090${hat.href}`
    const fetchConfig = {
        method: "delete"
    }
    const response = await fetch(hatDeleteUrl, fetchConfig);
    if (response.ok) {
        const data = await response.json()
        window.location.reload();

    }
}

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Style</th>
                        <th>Fabric</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map((hat, index) => {
                        return (
                            <tr key={hat.style_name + index}>
                                <td>{ hat.style_name }</td>
                                <td>{ hat.fabric }</td>
                                <td>{ hat.color }</td>
                                <td><img src={ hat.url } alt={ hat.style_name } className="img-thumbnail" heigh="200" width="200"/></td>
                                <td>{ hat.location.closet_name }</td>
                                <td>
                                    <button type="button" className="btn btn-danger" onClick={() => HatDelete(hat)}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>

            <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Hat</Link>
            </div>
        </>
    )
}

export default HatsList;

