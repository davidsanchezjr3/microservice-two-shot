import React, { useEffect, useState } from 'react'

function CreateHat () {
    const [locations, setLocations] = useState([]);
    const [styleName, setStyleName] = useState([]);
    const [fabric, setFabric] = useState([]);
    const [color, setColor] = useState([]);
    const [url, setUrl] = useState([]);
    const [location, setLocation] = useState([]);

    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleUrlChange = (event) => {
        const value = event.target.value;
        setUrl(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.style_name = styleName;
        data.fabric = fabric;
        data.color = color;
        data.url = url;
        data.location = location;

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation)

            setStyleName('');
            setFabric('');
            setColor('');
            setUrl('');
            setLocation('');
        }
    }




    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
            // console.log(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleStyleNameChange} value={styleName} placeholder="Style name" required
                            type="text" name="style_name" id="style_name"
                            className="form-control"/>
                            <label htmlFor="style_name">Style name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleUrlChange} value={url} placeholder="Url" type="url" name="url" id="url" className="form-control"/>
                            <label htmlFor="url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location.id} name="location" required id="location" className="form-select">
                            <option>Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.id}>
                                        {location.closet_name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateHat;
