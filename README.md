# Wardrobify

Team:

* Person 1 - Which microservice? Diana Hats
* Person 2 - Which microservice? David Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

For the Hats microservice, we'll have two models: Hat and LocationVO. The Hat model defines the characteristics of hat objects, including fabric, style name, color, picture, and location. It's important to note that hats can be associated with different locations, and this relationship is represented as a one-to-many connection with the location model from the Wardrobe API. Essentially, one location can store multiple hats.

To keep the Hat API microservice up-to-date with the latest location information, we need a poller. This poller will periodically fetch Location data from the Wardrobe API. The retrieved data will then be used to create or update instances of the LocationVO model. Think of the LocationVO model as copies or representations of the Location objects. These copies will enable the Hat microservice to work with the specific location information it needs.
