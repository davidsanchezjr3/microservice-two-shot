from django.db import models


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=100, null=True)

    def __str__(self):
        return f"{self.closet_name}"


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=1000, null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.model_name
